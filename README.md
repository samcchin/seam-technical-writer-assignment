# NoiseAware API Reference
The **NoiseAware API** has predictable resource-oriented URLS, returns JSON-encoded responses and uses standard HTTP response codes and verbs.
- HTTPS is required for all API requests.
- All times are served with the appropriate timezone.

The base URL to send all API requests is http://portal.apps.noiseaware.io/.


> 📝 **Note: API Access**
> To access these endpoints, you will need:
> - A valid NoiseAware Partner account.
> - To authenticate using the token generated from the [ConnectionURL](https://noiseaware.readme.io/reference/third-party-access#connectionurl-details).
>
> Learn more about getting access to the NoiseAware API endpoints in the [Third Party Access guide](https://noiseaware.readme.io/reference/third-party-access).
>

<details>
<summary>Third Party Access Diagram</summary>

![Third Party Access Diagram](/images/ThirdPartyAccess.png)
</details>


<br>

Questions? Contact support@noiseaware.io for any questions or feedback.

<br>

## Threshold
A **Threshold** is the limit of acceptable level of noise at a property. An alert is sent only if noise exceeds the Noise Risk Score (NRS) Value during the set quiet hours.
- The NRS limit can be customized for each hour of the day.
- Noise is measured as a trailing average over a few minutes, with alerts only made for sustained noise that is likely to continue.
- The default threshold values will be assigned based on property type (indoor/outdoor).
- Advised against setting thresholds outdoors, below 15 NRS or much higher than 40.



| Attribute      | Type |  Description |
| --------- | -------- |------- |
| `NRS`     | integer  |  (0-100) Noise Risk Score, contextual metric measuring how loud it is and how long it is loud for |
| `TriggerCount`  | integer| Number of breaches before an event is triggered, minimum 1  |
| `activityZoneID`  | integer|  Identifier of monitored area  |
| `startTime`  | integer| (start hour 0-24); Measured start time of threshold, alerts to be sent if NRS is exceeded after|
| `endTime`  | integer| (end hour 0-24); Measured end time of threshold, alerts to be sent if NRS is exceeded prior to |
| `timeZone`  | string| Timezone of threshold; default set to the parent property's timezone  |

<br>

More information on [how thresholds work](https://help.noiseaware.io/hc/en-us/articles/6751270452243-How-do-Thresholds-Work-#:~:text=Measuring%20noise%20outside%20can%20be,a%20neighbor%20mowing%20their%20lawn) and [Noise Risk Score (NRS)](https://help.noiseaware.io/hc/en-us/articles/360022534693-Noise-Risk-Score).

<br>

#### Threshold Object Example
```json
{
    "NRS": 70,
    "TriggerCount": 5,
    "activityZoneID": 6938736476723408000,
    "startTime": 22,
    "endTime": 7,
    "timeZone": "Pacific Standard Time"
}
```

<br>

### Threshold Endpoints
The base URL to send all Threshold API requests is http://portal.apps.noiseaware.io/thresholds/

| Name      | Method |  URL |Description |
|---|---|---|---|
| list Thresholds | GET |  `/list` | Returns a paginated list of Thresholds|
| replace Thresholds | PUT |  `/activityZone/{id}/replace` | Replaces existing Threshold based on activity zone ID, with updated information provided in the request body parameters  |

<br>


## List Thresholds
Returns a list of your Thresholds.

**GET**   http://portal.apps.noiseaware.io/thresholds **/list**

<br>

![List Thresholds Diagram](/images/ListThresholds.png)


### Parameters

**Header**

<details>
<summary>Authorization</summary>
<br>

| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------ |
| `Authorization`  | string  | required | Bearer <API_KEY>  is sent in the request header  |
</details>

<br>

**Body Parameters**
<details>
<summary>Filters</summary>


**Filtering** is an array of filter options to add basic filtering to the API. Each filter is one of 3 types, which specify which type of operator is available. The filter type is determined by the field and the endpoint.

| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------ |
| `key`     | string  | required | Field to filter by |
| `operator` | string  | required | Valid values are specified by the filter type |
| `value`     | string  | required | Value(s) to filter by determined based on field and operator |


**Filter types:**
- Valid Operators
    - Time '>=', '<='
    - String '=', '!=', `'IN'`, `'NOT IN'`
    - Number '=', '!=', '>=', '<=',`'IN'`, `'NOT IN'`


For a IN or NOT IN filter, a comma separated list is appropriate.

**Note:** This means that currently the IN and NOT IN operators do NOT support values with commas in the string EXCEPT as a separator.

</details>

<details>
<summary>Pagination</summary>
NoiseAware uses a single universal format of pagination. Each base item is optional.
<br>

| Field     | Type | Optional |  Description |
| --------- | -------- |------- | ------- |
| `pageNum` | integer  | optional | Page number of results to return |
| `perPage` | integer  | optional |  Number of items to return per page |

</details>


<details>
<summary>Search</summary>

**Search** allows you to do a text search of all fields specified by the term entered. The search term is optional.
<br>

| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------- |
| `search`     | string  | optional | Field to search string|

</details>


<details>
<summary>Sort</summary>

**Sort** allows you to sort the returned list of Thresholds, though some calls will only support a single sort.
<br>

| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------- |
| `key`     | string  | required | Field to sort by |
| `order`  | string | optional | Direction to order by; ASC for ascending; DSC for descending order  |

- `key` is required for a sort, and fields are specified by the endpoints information
- `order` is optional, and if it is not set, it will default to ASC (ASC and DESC are the valid values)

</details>

<br>

### Request
<details>
<summary>Request Example</summary>

```py
import requests

url = "http://portal.apps.noiseaware.io/thresholds/list"

payload = { "pagination": {
        "pageNum": 1,
        "perPage": 20
    } }
headers = {
    "accept": "application/vnd.threshold.list.response+json",
    "content-type": "application/json",
    "Authorization": "API-KEY-EXAMPLE"
}

response = requests.post(url, json=payload, headers=headers)

print(response.text)
```
</details>

<br>

### Response

Returns an object containing the following key-value pairs.
- A `data` key with an array value based on filtered and/or sorted by `key`. Each object entry in the array is a separate Threshold object. If no Thresholds are available, the resulting array will be empty.
- A `detail` key with a string value of the readable error.
- A `errors` key with an array value of error objects with details. Each object entry in the array is a separate error with a `detail` key with a string value of a readable explanation specific to the error, and a `field` key with a string value of the field error associated with it.
- A `pagination` key with an object value. The object contains a `pageNum` key with an integer value of the page number, a `perPage` key with an integer value of the page size, and a `total` key with an integer value of the total records possible.
- A `status` key with a string value of the HTTPS status code.


<br>

<details>
<summary>Example Response</summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "No errors returned.",
  "errors": [],
  "pagination": {
    "pageNum": 1,
    "perPage": 20,
    "total": 4000
  },
  "status": "200"
}
```
</details>


#### HTTP Status Code Response Examples

<details>
<summary>Status Codes</summary>
NoiseAware uses conventional HTTP response codes to indicate the success or failure of an API request.
<br>

| Code      | Response | Description |
| --------- |  ------- | ------- |
| `200`     | OK  | The request was successfully processed. |
| `400`  | Bad Request | The request was malformed or missing required parameters. |
| `401`  | Unauthorized | The API key provided was invalid or missing. |
| `500`  | Internal Server Error | An unexpected error occurred on the server.|

<details>
<summary>200: OK  </summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "No errors returned.",
  "errors": [],
  "pagination": {
    "pageNum": 1,
    "perPage": 20,
    "total": 4000
  },
  "status": "200"
}
```
</details>


<details>
<summary>400: Bad Request </summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "Bad Request",
  "errors": [
    {
      "detail": "The 'NRS' field is missing or invalid",
      "field": "NRS"
    },
    {
      "detail": "The 'TriggerCount' field is missing or invalid",
      "field": "TriggerCount"
    },
  ],
  "pagination": {
    "pageNum": 1,
    "perPage": 20,
    "total": 4000
  },
  "status": "400"
}
```
</details>

<details>
<summary>401: Unauthorized </summary>
<br>

```json
{
  "data": null,
  "detail": "Unauthorized",
  "errors": [
    {
      "detail": "No valid API key provided.",
      "field": "Authorization"
    }
  ],
  "status": "401"
}
```
</details>


<details>
<summary>500: Internal Server Error </summary>
<br>

```json
{
  "data": null,
  "detail": "Internal Server Error",
  "errors": [
    {
      "detail": "An unexpected error occurred on the server. Please try again later.",
      "field": "Internal error"
    }
  ],
  "status": "500"
}
```
</details>



</details>



<br>
<br>

## Replace Thresholds
Replaces  existing Threshold object for a specified activity zone.

**PUT**   http://portal.apps.noiseaware.io/thresholds **/activityZone/{id}/replace**


<br>

![Replace Thresholds Diagram](/images/ReplaceThresholds.png)

### Parameters

**Header**

<details>
<summary>Authorization</summary>
<br>

| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------ |
| `Authorization`  | string  | required | Bearer <API_KEY>  is sent in the request header  |
</details>

<br>

**Path Parameters**
<details>
<summary>Activity Zone ID</summary>


| Field      | Type |  Optional | Description |
| --------- | -------- |------- | ------ |
| `id`     | integer  | required | Activity zone ID |
- `id` is required for the URL path, it points to the specific activity zone that is to be replaced

</details>

<br>

**Body Parameters**

<details>
<summary>Threshold</summary>

The new **Threshold** will replace the existing Threshold identified by the activity zone ID.
<br>

| Field       | Type |  Optional | Description |
| --------- | -------- |------- |------- |
| `NRS`     | integer  | optional | (0-100) Noise Risk Score, contextual metric measuring how loud it is and how long it is loud for |
| `TriggerCount`  | integer| optional |Number of breaches before an event is triggered, minimum 1  |
| `activityZoneID`  | integer| optional | Identifier of monitored area  |
| `startTime`  | integer|optional | (start hour 0-24); Measured start time of threshold, alerts to be sent if NRS is exceeded after|
| `endTime`  | integer|optional | (end hour 0-24); Measured end time of threshold, alerts to be sent if NRS is exceeded prior to |
| `timeZone`  | string| optional |Timezone of threshold; default set to the parent property's timezone  |

</details>

<br>

### Request
<details>
<summary>Request Example</summary>

```py
import requests

url = "http://portal.apps.noiseaware.io/thresholds/activityZone/28/replace"

payload = { "thresholds": [
        {
            "TriggerCount": 1,
            "activityZoneID": "6938736476723408000",
            "endTime": 6,
            "startTime": 22,
            "NRS": 40,
            "timeZone": "Pacific Standard Time"
        }
    ] }
headers = {
    "accept": "application/vnd.threshold.replace.response+json",
    "content-type": "application/json",
    "Authorization": "API-KEY-EXAMPLE"
}

response = requests.put(url, json=payload, headers=headers)

print(response.text)
```
</details>


<br>

### Response

Returns the Threshold object containing the following key-value pairs. Any parameter(s) not provided will be left unchanged.
- A `data` key with an array value based on the specified activity zone ID provided. Each object entry in the array is a separate Threshold object.
- A `detail` key with a string value of the readable error.
- A `errors` key with an array value of error objects with details. Each object entry in the array is a separate error with a `detail` key with a string value of a readable explanation specific to the error, and a `field` key with a string value of the field error associated with it.
- A `status` key with a string value of the HTTPS status code.


<br>

<details>
<summary>Example Response</summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "No errors returned.",
  "errors": [],
  "status": "200"
}
```
</details>

#### HTTP Status Code Response Examples

<details>
<summary>Status Codes</summary>
NoiseAware uses conventional HTTP response codes to indicate the success or failure of an API request.
<br>

| Code      | Response | Description |
| --------- |  ------- | ------- |
| `200`     | OK  | The request was successfully processed. |
| `400`  | Bad Request | The request was malformed or missing required parameters. |
| `401`  | Unauthorized | The API key provided was invalid or missing. |
| `500`  | Internal Server Error | An unexpected error occurred on the server.|

<details>
<summary>200: OK  </summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "No errors returned.",
  "errors": [],
  "status": "200"
}
```
</details>


<details>
<summary>400: Bad Request </summary>
<br>

```json
{
  "data": [
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    },
    {
      "NRS": 860524306370836900,
      "TriggerCount": 8892729748833146000,
      "activityZoneID": 6938736476723408000,
      "endTime": 5512238172436655000,
      "startTime": 2429264096241431000,
      "timeZone": "Animi laboriosam harum."
    }
  ],
  "detail": "Bad Request",
  "errors": [
    {
      "detail": "The 'NRS' field is missing or invalid",
      "field": "NRS"
    },
    {
      "detail": "The 'TriggerCount' field is missing or invalid",
      "field": "TriggerCount"
    },
  ],
  "status": "400"
}
```
</details>

<details>
<summary>401: Unauthorized </summary>
<br>

```json
{
  "data": null,
  "detail": "Unauthorized",
  "errors": [
    {
      "detail": "No valid API key provided.",
      "field": "Authorization"
    }
  ],
  "status": "401"
}
```
</details>


<details>
<summary>500: Internal Server Error </summary>
<br>

```json
{
  "data": null,
  "detail": "Internal Server Error",
  "errors": [
    {
      "detail": "An unexpected error occurred on the server. Please try again later.",
      "field": "Internal error"
    }
  ],
  "status": "500"
}
```
</details>



</details>

<br>
